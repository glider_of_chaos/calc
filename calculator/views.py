from django.shortcuts import render
from django.http import HttpResponse

import re

def calculator(request):
    # eval()
    return render(request, 'base.html')
    
def result(request):
    eval_string = request.GET.get('estr', '')
    
    if re.search(r'[^0-9/+*\.() -]', eval_string):
        res = "string contains unexpected symbols"
    else:
        try:
            res = str(eval(eval_string))
        except:
            res = "not a proper expression"
    return HttpResponse(res)
