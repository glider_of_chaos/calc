from django.test import TestCase
from django.test.client import Client

class CalculationTests(TestCase):

    def test_literals_in_expression(self):
        """
        If user manages to give string with literals it react properly
        """
        
        self.client = Client()
        response = self.client.get('/result/', { 'estr' : 'abc + 2' })
        
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "string contains unexpected symbols")
        
    def test_messed_expression(self):
        """
        If user produces messed up string using correct symbols
        """
        
        self.client = Client()
        response = self.client.get('/result/', { 'estr' : '-+/2-3+' })
        
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "not a proper expression")
        
    def test_proper_expression(self):
        """
        If it's a perfectly valid string
        """
        
        self.client = Client()
        response = self.client.get('/result/', {'estr' : '4000+87*10+1-2/(3+5)'})
        
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "4870.75")
