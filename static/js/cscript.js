var need_to_clear = false;

function clearInput(){
    document.getElementById("input_field").value = '';
    need_to_clear = false;
}

function addNumber(e){
    if (need_to_clear){
        clearInput();
    }
    var number_to_add = e.target.innerHTML;
    var current_input = document.getElementById("input_field").value;
    
    if (number_to_add == '.'){
        var need_zero = /\d$/;
        if (!need_zero.test(current_input)){
            document.getElementById("input_field").value += '0'
        }
    }
    document.getElementById("input_field").value += number_to_add;
}

function getResult(){
    var string_to_eval = document.getElementById("input_field").value;
    var get_response = new XMLHttpRequest();
    var url_with_get = 'result/?estr=' + encodeURIComponent(string_to_eval);
    //can't go with asynchrounous as it's slow to react and kills next operator
    get_response.open("GET", url_with_get, false);
    get_response.send();
    document.getElementById("input_field").value = get_response.responseText;
    need_to_clear = true;
}


function addOperator(e){
    var new_operator = e.target.innerHTML;
    var current_input = document.getElementById("input_field").value;
    var finished_expression = /\d[+\/*-]-?\d/;
    
    if (finished_expression.test(current_input)){
        getResult();
    }
    else{
        var expression_in_progress = current_input.search(/[+\/*-]$/);
        if (expression_in_progress != -1){
            document.getElementById("input_field").value =
                document.getElementById("input_field").value.slice(0, expression_in_progress);
                
        }
        
    }
    
    var last_digit = /\d$/;
    if (last_digit.test(document.getElementById("input_field").value)){
        document.getElementById("input_field").value += new_operator;
    }

    need_to_clear = false;
}

function plusMinus(){
    var current_input = document.getElementById("input_field").value;
    var insert_minus = current_input.search(/[\d\.]+$/);
    if (insert_minus != -1){
        current_input = current_input.slice(0,insert_minus) + '-' +
            current_input.slice(insert_minus);
    }
    var double_minus = /\d--\d/;
    if (!double_minus.test(current_input)){
        current_input = current_input.replace(/--/, '');
    }
    document.getElementById("input_field").value = current_input;
}
