from django.conf.urls import patterns, include, url
from django.contrib import admin
from calculator import views

urlpatterns = patterns('',

    url(r'^$', 'calculator.views.calculator'),
    url(r'^result/$', 'calculator.views.result'),
    
    url(r'^admin/', include(admin.site.urls)),
)
